#!/bin/bash

MY_PROJECT_NAME="puppet-nextcloud"
MY_MODULE_URL="git@gitlab.com:matt.faure/${MY_PROJECT_NAME}.git"
MY_REFSPEC="master"
MY_PUPPET_MODULE_NAME="nextcloud"

r10k puppetfile install -v \
    && cd modules \
    && git clone "${MY_MODULE_URL}" \
    && mv "${MY_PROJECT_NAME}" "${MY_PUPPET_MODULE_NAME}" \
    && cd "${MY_PUPPET_MODULE_NAME}" \
    && git checkout "${MY_REFSPEC}"


