# vagrant-nextcloud

Vagrant box to initiate development of a Puppet module for Nextcloud

## Prerequisites

* Virtualbox 
* Vagrant
* git
* r10k (see below how to do this properly)

## r10k installation on a workstation

TODO: this should be correctly explained and tested (which by now may not be the case :) )

1. On your workstation, install [RBenv](https://github.com/rbenv/rbenv). RBenv allows a user to have different versions of Ruby running on a system and isolate them. This is a great help when using Puppet on Linux, as Ruby versions shipped with distros can be quite old. Moreover, RBenv allows user to have a Ruby installation in its user-space, so that no root privileges are required.
1. With RBenv install Ruby 2.5.x (please follow RBenv documentation to do that)
1. Install r10k: `gem install r10k`

## Usage

Grab this repository, get into it, grab Puppet modules needed and then run Vagrant:

```
git clone https://gitlab.com/matt.faure/vagrant-nextcloud.git
cd vagrant-nextcloud
./BUILD.sh
```
