$apache_docroot = '/var/www/html'
$_phpinfo = "${apache_docroot}/phpinfo.php"
$_phpinfo_local = '/tmp/phpinfo.html'

class { 'nextcloud':
  manage_apache  => true,
  apache_docroot => $apache_docroot
}

file { $_phpinfo:
  path   => $_phpinfo,
  ensure => file,
  owner  => 'www-data',
  mode   => '0644',
}
file_line { 'phpinfo':
  path => $_phpinfo,
  line => '<?php phpinfo(); ?>',
}

archive { $_phpinfo_local:
  ensure => present,
  source => 'http://localhost/phpinfo.php',
}

# Ensure PHP is running + PHP engine is FPM
file_line { 'php-fpm':
  path               => $_phpinfo_local,
  line               => '<tr><td class="e">Server API </td><td class="v">FPM/FastCGI </td></tr>',
  replace            => false,
  append_on_no_match => false,
}
